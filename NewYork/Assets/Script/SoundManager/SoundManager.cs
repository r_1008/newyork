﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    private static SoundManager instance;

    public static SoundManager Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject("SoundManager");
                instance = go.AddComponent<SoundManager>();
            }

            return instance;
        }





    }

    [SerializeField]
    public AudioSource audioSourceSE;
    [SerializeField]
    public AudioSource audioSourceBGM;

    [SerializeField]
    public AudioClip[] SEs;


    [SerializeField]
    public AudioClip[] BGMs;

    public void Start()
    {
        instance = this;
    }

    public void PlaySE(int num)
    {
        audioSourceSE.clip = SEs[num];
        audioSourceSE.Play();
    }

    public void PlayBGM(int num)
    {
        audioSourceBGM.clip = BGMs[num];
        audioSourceBGM.Play();
    }

}
