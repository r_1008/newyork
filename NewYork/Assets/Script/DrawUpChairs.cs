﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawUpChairs : MonoBehaviour {
    public bool hit;
    public GameObject player;
    public int count;
    public Vector3 posi;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if(hit)
        {
            posi = player.transform.position - (player.transform.forward * 20);
            transform.position = player.transform.position - player.transform.forward * count;
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if(!hit && other.tag.Equals("hit"))
        {
            count = player.GetComponent<SphereControl>().hit();
            hit = true;
        }
    }
}
